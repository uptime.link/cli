/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@uptime.link/cli',
  version: '1.0.2',
  description: 'the cli for uptime.link'
}
